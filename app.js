'use strict';

let showResult = document.getElementById('resultBox');
let showResultText = document.getElementById('transferredValue');
let clear = document.getElementById('remove');
let input = document.getElementById('inputField');

input.addEventListener('focus', function() {
    input.style.cssText='outline: none !important; border: 2px solid #008000; box-shadow: 0 0 10px;';
}, true);
input.addEventListener('blur', blur, true);
clear.addEventListener('click', removeContent);

function blur() {
    input.style = input;
    showResult.style.visibility = 'visible';
    showResultText.innerText = 'Current price is $' + `${input.value}`;

    if (input.value <= 0 || isNaN(input.value)) {

        if (document.getElementById('error')) {
            document.getElementById('error').remove();
        }
        showResult.style.visibility = 'hidden';
        input.style.backgroundColor = '#f08080';
        input.insertAdjacentHTML('afterend', '<span id="error" class="error">Please enter correct price</span>');
    } else if (document.getElementById('error')) {
        document.getElementById('error').remove();
    } else input.style.backgroundColor='#008000';
}

function removeContent() {
    showResultText.value='';
    input.value='';
    showResult.style.visibility='hidden';
    input.style.backgroundColor='white';
}


